﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NMeijer.SceneLoading
{
	public abstract class LoadingSceneView : MonoBehaviour
	{
		#region Lifecycle

		private void Start()
		{
			StartCoroutine(SceneLoadRoutine());
		}

		#endregion

		#region Protected Methods

		protected abstract IEnumerator FadeInRoutine();

		protected abstract IEnumerator FadeOutRoutine();

		protected abstract void UpdateProgress(float progress);

		#endregion

		#region Private Methods

		private IEnumerator SceneLoadRoutine()
		{
			if(!SceneLoader.CurrentSceneRequest.SkipFadeIn)
			{
				yield return StartCoroutine(FadeInRoutine());
			}

			yield return SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
			AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(SceneLoader.CurrentSceneRequest.ScenePath, LoadSceneMode.Additive);

			// Unity's scene load progress goes from 0 to 0.9
			while(asyncOperation.progress < 0.9f)
			{
				UpdateProgress(asyncOperation.progress / 0.9f);
			}

			// Wait the last bit (asyncOperation.isDone seems to never switch to true)
			yield return asyncOperation;

			SceneManager.SetActiveScene(SceneManager.GetSceneByName(SceneLoader.CurrentSceneRequest.ScenePath));
			yield return StartCoroutine(FadeOutRoutine());

			SceneLoader.CurrentSceneRequest.Finish();

			SceneManager.UnloadSceneAsync(SceneLoader.CurrentSceneRequest.LoadingScenePath);
		}

		#endregion
	}
}