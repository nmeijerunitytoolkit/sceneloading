# Scene Loading

### Description
An easy to use system for changing scenes with a loading screen in between whilst allowing for passing along data.

### Usage
To switch scenes `SceneLoader.RequestSceneChange` can be called. This function requires a `SceneRequest`. One can be made by inheriting from this class. It requires the following implementations:
* `ScenePath`: The `string` path to the scene you want to switch to
* `LoadingScenePath`: The `string` path to the scene you want to use as loading scene

In this class you can also implement the extra data to transfer between scenes. To get this data in the next scene, call `SceneLoader.TryGetCurrentSceneRequest` with the type of your `SceneRequest` implementation.

### Installation
This package can be installed directly with Git. For more information: https://docs.unity3d.com/Manual/upm-ui-giturl.html